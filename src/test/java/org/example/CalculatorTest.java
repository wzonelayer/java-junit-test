package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {

    Calculator getInstance() {
        return new Calculator();
    }

    @Test
    public void testAdd() {
        int num1 = 1;
        int num2 = 1;
        assertEquals(num1 + num2, getInstance().add(num1, num2));
    }
}